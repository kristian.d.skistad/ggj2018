﻿using System.Collections;
using UnityEngine;

namespace SparkTools
{
    public abstract class Singleton<T> where T : class, new()
    {
        private static T _instance;

        private static object _lock = new object();

        public static T instance
        {
            get
            {
                lock (_lock)
                {
                    if (_instance == null)
                    {
                        _instance = new T();

                        Debug.Log("An instance of " + typeof(T).ToString() + " is needed, so a singleton was created.");
                    }

                    return _instance;
                }
            }
        }
    }
}