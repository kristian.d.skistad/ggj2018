﻿using System.Collections;
using UnityEngine;


namespace SparkTools
{
    public abstract class SingletonComponent<T> : MonoBehaviour where T : MonoBehaviour
    {
        private static T _instance;

        private static object _lock = new object();

        public static T instance
        {
            get
            {
                if (applicationIsQuitting)
                {
                    Debug.LogWarning("Instance '" + typeof(T) +
                        "' already destroyed on application quit." +
                        " Won't create again - returning null.");
                    return null;
                }

                lock (_lock)
                {
                    if (_instance == null)
                    {
                        _instance = (T)FindObjectOfType(typeof(T));

                        if (FindObjectsOfType(typeof(T)).Length > 1)
                        {
                            Debug.LogError("Something went really wrong " +
                                " - there should never be more than 1 singleton!" +
                                " Reopening the scene might fix it.");
                            return _instance;
                        }

                        if (_instance == null)
                        {
                            string[] compoundName = typeof(T).ToString().Split('.');
                            GameObject singleton = new GameObject(compoundName[compoundName.Length - 1]);
                            _instance = singleton.AddComponent<T>();

                            DontDestroyOnLoad(singleton);
                            /*
                            SparkDebug.LogVerbose(typeof(SingletonComponent<T>), "An instance of " + typeof(T) +
                                " is needed in the scene, so '" + singleton +
                                "' was created with DontDestroyOnLoad."); */
                        }
                        else
                        {
                            /*
                            SparkDebug.LogVerbose(typeof(SingletonComponent<T>), "Using instance already created: " + _instance.gameObject.name);
                            */
                        }
                    }

                    return _instance;
                }
            }
        }

        protected virtual void Awake()
        {
            if (_instance != null)
                Destroy(this);

            DontDestroyOnLoad(this);
        }

        private static bool applicationIsQuitting = false;
        /// <summary>
        /// When Unity quits, it destroys objects in a random order.
        /// In principle, a Singleton is only destroyed when application quits.
        /// If any script calls Instance after it have been destroyed, 
        ///   it will create a buggy ghost object that will stay on the Editor scene
        ///   even after stopping playing the Application. Really bad!
        /// So, this was made to be sure we're not creating that buggy ghost object.
        /// </summary>
        public void OnDestroy()
        {
            applicationIsQuitting = true;
        }
    }
}