﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Preloader : MonoBehaviour
{
    private void Start()
    {
        //Make all the GameObjects in the preloader scene not be destroyed on load, aside from itself
        GameObject[] persistentObjects = gameObject.scene.GetRootGameObjects();
        for (int i=0; i< persistentObjects.Length; i+=1)
        {
            if (persistentObjects[i] != gameObject)
                DontDestroyOnLoad(persistentObjects[i]);
        }
        

        //move to the next scene, unless multiple scenes are open
        if (SceneManager.sceneCount == 1)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
        else //multiple scenes
        {
            //if the preloader scene is the current active scene, set the first loaded scene found to the active scene
            if (SceneManager.GetActiveScene() == gameObject.scene)
            {
                int i = 0;
                while (SceneManager.GetSceneAt(i) == gameObject.scene)
                    i += 1;
                SceneManager.SetActiveScene(SceneManager.GetSceneAt(i));
            }

            //unload the preloader scene
            SceneManager.UnloadSceneAsync(gameObject.scene);
        }
    }
}