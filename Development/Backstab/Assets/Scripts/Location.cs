﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Location", menuName = "Location", order = 100)]
public class Location : ScriptableObject
{
    public string locationName;
    public string description;
    public Sprite background;
}