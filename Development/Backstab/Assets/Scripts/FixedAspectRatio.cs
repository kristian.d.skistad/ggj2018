﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(AspectRatioFitter))]
[RequireComponent(typeof(CanvasScaler))]
public class FixedAspectRatio : MonoBehaviour
{
    private AspectRatioFitter aspectRatioFitter;
    private CanvasScaler canvasScaler;

    private float targetAspectRatio = 1.77757f;

    private int prevWidth = -1;
    private int prevHeight = -1;

    private void Awake()
    {
        aspectRatioFitter = GetComponent<AspectRatioFitter>();
        canvasScaler = GetComponent<CanvasScaler>();

        prevWidth = Screen.width;
        prevHeight = Screen.height;

        UpdateAspectRatio();
    }

    private void Update()
    {
        if (Screen.width != prevWidth || Screen.height != prevHeight)
            UpdateAspectRatio();

        prevWidth = Screen.width;
        prevHeight = Screen.height;
    }

    private void UpdateAspectRatio()
    {
        float aspectRatio = Screen.width / Screen.height;
        aspectRatioFitter.aspectRatio = targetAspectRatio;

        if (aspectRatio >= targetAspectRatio)
        {
            aspectRatioFitter.aspectMode = AspectRatioFitter.AspectMode.HeightControlsWidth;
            canvasScaler.matchWidthOrHeight = 1f;
        }
        else
        {
            aspectRatioFitter.aspectMode = AspectRatioFitter.AspectMode.WidthControlsHeight;
            canvasScaler.matchWidthOrHeight = 0f;
        }
    }
}