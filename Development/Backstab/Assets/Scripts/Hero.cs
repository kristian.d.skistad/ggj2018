﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hero
{
    public string name { get; private set; }
    public Sprite portrait { get; private set; }
    public List<Item> inventory = new List<Item>();

    public int health
    {
        get { return _health; }
        set { _health = Mathf.Clamp(value, 0, 100); }
    }
    private int _health = 100;

    public Stats stats = new Stats();

    public int trust
    {
        get { return _trust; }
        set { _trust = Mathf.Clamp(value, 0, 100); }
    }
    private int _trust = 100;

    public struct Stats
    {
        public int strength
        {
            get { return _strength; }
            set { _strength = Mathf.Clamp(value, 1, 10); }
        }
        private int _strength;

        public int dexterity
        {
            get { return _dexterity; }
            set { _dexterity = Mathf.Clamp(value, 1, 10); }
        }
        private int _dexterity;

        public int constitution
        {
            get { return _constitution; }
            set { _constitution = Mathf.Clamp(value, 1, 10); }
        }
        private int _constitution;

        public int intelligence
        {
            get { return _intelligence; }
            set { _intelligence = Mathf.Clamp(value, 1, 10); }
        }
        private int _intelligence;

        public int wisdom
        {
            get { return _wisdom; }
            set { _wisdom = Mathf.Clamp(value, 1, 10); }
        }
        private int _wisdom;

        public int charisma
        {
            get { return _charisma; }
            set { _charisma = Mathf.Clamp(value, 1, 10); }
        }
        private int _charisma;
    }

    public void RollForStats()
    {
        stats.strength = Random.Range(1, 10);
        stats.dexterity = Random.Range(1, 10);
        stats.constitution = Random.Range(1, 10);
        stats.intelligence = Random.Range(1, 10);
        stats.wisdom = Random.Range(1, 10);
        stats.charisma = Random.Range(1, 10);
    }



    public void SetName(string name)
    {
        this.name = name;
    }

    public void SetPortrait(Sprite image)
    {
        portrait = image;
    }

    public bool hasItem(string itemName)
    {
        foreach(Item item in inventory)
        {
            if (itemName.Equals(item.name))
            {
                return true;
            }
        }
        return false;
    }
}