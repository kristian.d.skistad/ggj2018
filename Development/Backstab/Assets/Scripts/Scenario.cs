﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Scenario", menuName = "Scenario", order = 100)]
public class Scenario : ScriptableObject
{
    [TextArea]
    public string[] text;
    public Location location;
    public Choice[] choices;
    //public InteractableImage[] interactableImages;

    [System.Serializable]
    public class Choice
    {
        public enum StatType { Strength, Dexterity, Constitution, Intelligence, Wisdom, Charisma }

        [TextArea]
        public string text;
        //public int interactableIndex;
        [Space]
        public StatType stat;
        [Range(0, 10)]
        public int value;
        [Space]
        public Result successResult;
        public Result failResult;

        [System.Serializable]
        public class Result
        {
            public string[] text;
            [Space]
            public int healthChange;
            [Space]
            public int strengthChange;
            public int dexterityChange;
            public int constitutionChange;
            public int intelligenceChange;
            public int wisdomChange;
            public int charismaChange;
            public int trustChange;
            [Space]
            public int scenarioIndex = -1;

            public string toStringText()
            {
                string temp = "";
                foreach(string line in text)
                {
                    temp += line + "\n";
                }
                return temp;
            }
        }
    }

    [System.Serializable]
    public class InteractableImage
    {
        public Sprite image;
        public Vector2 position;
    }
}