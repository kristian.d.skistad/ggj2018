﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExtendedText : MonoBehaviour {

    public TextAsset runOnText;
    public Text textbox;
    public Actions[] acting;

    private string[] textSets;
    private int clicks;

    // Use this for initialization
    public void Start ()
    {
        this.textSets = runOnText.text.Split('\n');
        textbox.text = this.textSets[clicks];
    }
	
	public void nextText()
    {
        if(!isDone())
        {
            clicks++;
            textbox.text = this.textSets[clicks];
        }

        foreach (Actions action in acting)
        {
            if (action.timing.Contains(clicks))
            {
                action.item.SetActive(true);
            }
            else
            {
                action.item.SetActive(false);
            }
        }
    }

    public bool isDone()
    {
        return this.clicks >= (this.textSets.Length-1);
    }

    [System.Serializable]
    public class Actions
    {
        public List<int> timing;
        public GameObject item;
    }
}
