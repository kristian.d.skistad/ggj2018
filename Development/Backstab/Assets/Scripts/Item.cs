﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item {

    public string name { get; private set; }
    int iID;
    bool stackable;
    int stack;

    public Item(string name)
    {
        this.name = name;
        this.stackable = false;
        this.stack = 0;
    }

    public Item(string name, int stack)
    {
        this.name = name;
        this.stackable = true;
        this.stack = stack;
    }

    public void cloneItem(Item item)
    {
        this.name = item.name;
        this.iID = item.iID;
        this.stackable = item.stackable;
        this.stack = item.stack;
    }

    public void setName(string name)
    {
        this.name = name;
    }

    public string getName()
    {
        return this.name;
    }

    public void setIID(int iID)
    {
        this.iID = iID;
    }

    public int getIID()
    {
        return this.iID;
    }
}
