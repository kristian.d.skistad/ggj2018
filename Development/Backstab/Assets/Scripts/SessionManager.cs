﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SessionManager : MonoBehaviour
{
    public Scenario[] scenarios;
    private const int startTime = 30;
    public int timeLeft = startTime;
    [Space]
    public TextAsset maleNames;
    public TextAsset femaleNames;
    public Sprite[] malePortraits;
    public Sprite[] femalePortraits;
    [Space]
    public Image portrait;
    public Text textbox;
    public Transform choiceFolder;
    public GameObject choicePrefab;
    public Image background;
    public Transform interactablesFolder;
    public Text stats;
    public BarTween healthBar;
    public BarTween trustBar;
    public BarTween timeBar;
    public GameObject resultTextObj;
    public Text resultText;
    public Scenario intro;
    public Scenario win;
    public Scenario loseTrust;
    public Scenario loseTime;

    public Hero hero { get; private set; }
    public Scenario scenario { get; private set; }

    private List<int> freeScenarios = new List<int>();

    private void Start()
    {
        SetupSession();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            background.color = new Color(background.color.r, background.color.g, background.color.b, .5f);
        }

        if (Input.GetKeyUp(KeyCode.LeftControl))
        {
            background.color = new Color(background.color.r, background.color.g, background.color.b, 1f);
        }
    }

    public void SetupSession()
    {
        //generate a hero
        hero = GenerateHero();
        portrait.sprite = hero.portrait;
        UpdateStats();

        //retrieve free scenarios
        for (int i = 0; i < scenarios.Length; i += 1)
            freeScenarios.Add(i);

        //set the scenario
        StartScenario(intro);
    }

    public Hero GenerateHero()
    {
        Hero hero = new Hero();
        bool male = Random.Range(0f, 1f) > 2f/7f ? true : false;

        if (male)
        {
            string[] names = maleNames.text.Split('\n');
            hero.SetName(names[Random.Range(0, names.Length)]);
            hero.SetPortrait(malePortraits[Random.Range(0, malePortraits.Length)]);
        }
        else
        {
            string[] names = femaleNames.text.Split('\n');
            hero.SetName(names[Random.Range(0, names.Length)]);
            hero.SetPortrait(femalePortraits[Random.Range(0, femalePortraits.Length)]);
        }

        hero.RollForStats();

        return hero;
    }

    public void StartScenario(int index)
    {
        if (index < 0 || index >= scenarios.Length)
            return;

        //set the active scenario
        scenario = scenarios[index];

        //remove the scenario from the free scenarios
        freeScenarios.Remove(index);

        //set the textbox
        if (scenario.text.Length > 0)
            textbox.text = scenario.text[0].Replace("{hero}", hero.name);

        //set the background
        if (scenario.location != null)
            background.sprite = scenario.location.background;

        //remove old choices
        foreach (Transform child in choiceFolder)
            Destroy(child.gameObject);

        //set choices
        for (int i=0; i<scenario.choices.Length; i+=1)
        {
            GameObject obj = Instantiate(choicePrefab, choiceFolder);
            obj.GetComponentInChildren<Text>().text = scenario.choices[i].text.Replace("{hero}", hero.name);
            int n = i;
            obj.GetComponent<Button>().onClick.AddListener(() => { MakeChoice(scenario.choices[n]); });
        }

        /*
        //Delete old interactables
        foreach (Transform child in interactablesFolder)
            Destroy(child.gameObject);

        //create new interactables
        for (int i=0; i<scenario.interactableImages.Length; i+=1)
        {
            GameObject obj = new GameObject(scenario.interactableImages[i].image.name);
            obj.transform.SetParent(interactablesFolder);
            obj.transform.localScale = Vector3.one;
            obj.transform.localPosition = scenario.interactableImages[i].position;
            Image img = obj.AddComponent<Image>();
            img.sprite = scenario.interactableImages[i].image;
            obj.GetComponent<RectTransform>().sizeDelta = new Vector2(img.sprite.rect.width, img.sprite.rect.height);

            obj.AddComponent<Outline>();
        }
        */
    }

    public void StartScenario(Scenario s)
    {
        //set the active scenario
        scenario = s;

        //set the textbox
        if (scenario.text.Length > 0)
            textbox.text = scenario.text[0].Replace("{hero}", hero.name);

        //set the background
        if (scenario.location != null)
            background.sprite = scenario.location.background;

        //remove old choices
        foreach (Transform child in choiceFolder)
            Destroy(child.gameObject);

        //set choices
        for (int i = 0; i < scenario.choices.Length; i += 1)
        {
            GameObject obj = Instantiate(choicePrefab, choiceFolder);
            obj.GetComponentInChildren<Text>().text = scenario.choices[i].text.Replace("{hero}", hero.name);
            int n = i;
            obj.GetComponent<Button>().onClick.AddListener(() => { MakeChoice(scenario.choices[n]); });
        }
    }

        private bool SkillCheck(int stat, int check)
    {
        float chance = Mathf.Min(9, 10 - check + stat - 3) / 10f;
        return (Random.Range(0f, 1f) <= chance);
    }

    private void MakeChoice(Scenario.Choice choice)
    {
        if (scenario == win)
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene("Win");
            return;
        }

        if (scenario == loseTrust || scenario == loseTime)
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene("Lose");
            return;
        }

        if (scenario != intro)
        {
            int stat = 0;
            switch (choice.stat)
            {
                case Scenario.Choice.StatType.Strength:
                    stat = hero.stats.strength;
                    break;

                case Scenario.Choice.StatType.Dexterity:
                    stat = hero.stats.dexterity;
                    break;

                case Scenario.Choice.StatType.Constitution:
                    stat = hero.stats.constitution;
                    break;

                case Scenario.Choice.StatType.Intelligence:
                    stat = hero.stats.intelligence;
                    break;

                case Scenario.Choice.StatType.Wisdom:
                    stat = hero.stats.wisdom;
                    break;

                case Scenario.Choice.StatType.Charisma:
                    stat = hero.stats.charisma;
                    break;
            }

            Scenario.Choice.Result result;
            if (SkillCheck(stat, choice.value))
                result = choice.successResult;
            else
                result = choice.failResult;

            hero.health += result.healthChange;
            hero.trust += result.trustChange;
            hero.stats.strength += result.strengthChange;
            hero.stats.dexterity += result.dexterityChange;
            hero.stats.constitution += result.constitutionChange;
            hero.stats.intelligence += result.intelligenceChange;
            hero.stats.wisdom += result.wisdomChange;
            hero.stats.charisma += result.charismaChange;
            timeLeft -= 1;

            resultText.text = "\"" + choice.text.Replace("{hero}", hero.name) + "\"\n\n" + result.toStringText();
            resultTextObj.SetActive(true);

            UpdateStats();

            if (hero.health <= 0f)
            {
                StartScenario(win);
                return;
            }

            if (hero.trust <= 0f)
            {
                StartScenario(loseTrust);
                return;
            }

            if (timeLeft <= 0f)
            {
                StartScenario(loseTime);
                return;
            }

            if (result.scenarioIndex >= 0 && result.scenarioIndex < scenarios.Length)
            {
                StartScenario(result.scenarioIndex);
                return;
            }
        }

        //pick new scenarios
        List<int> picks = new List<int>();

        if (freeScenarios.Count == 0)
            GetFreeScenario();

        int choices = Mathf.Min(freeScenarios.Count, Random.Range(2, 4));

        for (int i = 0; i < choices; i += 1)
            picks.Add(GetFreeScenario(picks.ToArray()));

        for (int i = 0; i < picks.Count; i += 1)
        {
            for (int j = i + 1; j < picks.Count; j += 1)
            {
                if (scenarios[picks[i]].location == scenarios[picks[j]].location)
                {
                    picks.RemoveAt(j);
                }
            }
        }

        //set the textbox
        if (scenario.text.Length > 0)
        {
            textbox.text = "Up ahead it looks like there's ";

            if (picks.Count > 2)
            {
                for (int i = 0; i < picks.Count; i += 1)
                {
                    textbox.text += scenarios[i].location.description;

                    if (i != picks.Count - 1)
                        textbox.text += ", ";

                    if (i == picks.Count - 2)
                        textbox.text += "and ";
                }

                textbox.text += ". Where should we go?";
            }
            else if (picks.Count == 2)
            {
                textbox.text += scenarios[0].location.description + " and " + scenarios[1].location.description;
                textbox.text += ". Where should we go?";
            }
            else if (picks.Count == 1)
            {
                textbox.text += "just " + scenarios[0].location.description + ".";
            }
        }

        //remove old choices
        foreach (Transform child in choiceFolder)
            Destroy(child.gameObject);

        //set choices
        for (int i = 0; i < picks.Count; i += 1)
        {
            GameObject obj = Instantiate(choicePrefab, choiceFolder);
            if (picks.Count > 1)
                obj.GetComponentInChildren<Text>().text = scenarios[i].location.locationName;
            else
                obj.GetComponentInChildren<Text>().text = "Continue";
            int n = picks[i];
            obj.GetComponent<Button>().onClick.AddListener(() => { StartScenario(n); });
        }
    }

    public int GetFreeScenario()
    {
        //refill the pool if empty
        if (freeScenarios.Count == 0)
        {
            for (int i = 0; i < scenarios.Length; i += 1)
            {
                freeScenarios.Add(i);
            }
        }

        return freeScenarios[Random.Range(0, freeScenarios.Count)];
    }

    public int GetFreeScenario(int[] blacklist)
    {
        List<int> readd = new List<int>();
        for (int i = 0; i < blacklist.Length; i += 1)
        {
            if (freeScenarios.Remove(blacklist[i]))
                readd.Add(blacklist[i]);
        }

        int n = GetFreeScenario();

        for (int i = 0; i < readd.Count; i += 1)
        {
            freeScenarios.Add(readd[i]);
        }

        return n;
    }

    public void UpdateStats()
    {
        healthBar.value = hero.health / 100f;
        trustBar.value = hero.trust / 100f;
        timeBar.value = (float)(startTime - timeLeft) / (float)startTime;

        stats.text =
            "Strength: " + hero.stats.strength + "\n" + 
            "Dexterity: " + hero.stats.dexterity + "\n" +
            "Constitution: " + hero.stats.constitution + "\n" +
            "Intelligence: " + hero.stats.intelligence + "\n" +
            "Wisdom: " + hero.stats.wisdom + "\n" +
            "Charisma: " + hero.stats.charisma;
    }
}