﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarTween : MonoBehaviour
{
    public RectTransform walkingIcon;

    public float value
    {
        get { return _value; }
        set { _value = Mathf.Clamp01(value); }
    }
    private float _value = 1f;

    private UnityEngine.UI.Image bar;

    private void Awake()
    {
        bar = GetComponent<UnityEngine.UI.Image>();
    }

    private void Update()
    {
        if (bar.fillAmount != value)
        {
            bar.fillAmount = Mathf.Lerp(bar.fillAmount, value, 5f * Time.deltaTime);

            if (walkingIcon)
                walkingIcon.localPosition = new Vector3(bar.rectTransform.rect.width * bar.fillAmount + 5f, walkingIcon.localPosition.y, walkingIcon.localPosition.z);
        }
    }
}